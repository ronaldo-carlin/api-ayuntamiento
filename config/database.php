<?php

return [
    'default' => 'produccion',

    'connections' => [
        'produccion' => [
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => 'u286806953_comunidad_mix',
            'username'  => 'u286806953_localhost',
            'password'  => '22-25ComunidadMix',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ],
        'pruebas' => [
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => 'u286806953_comunidad_prue',
            'username'  => 'u286806953_pruebas',
            'password'  => '22-25PresidenciaMix',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ],
        'localhost' => [
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => 'comunidad_mix',
            'username'  => 'root',
            'password'  => '',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ]
    ],
];

?>
