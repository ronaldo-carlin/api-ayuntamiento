<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return 'Proyecto Arriba';
});

//! Usuarios
$router->post('createUser', 'UsersController@createUser');
$router->post('deleteUser', 'UsersController@deleteUser');
$router->post('updateUser', 'UsersController@updateUser');
$router->post('updatePassword', 'UsersController@updatePassword');
$router->get('login', 'UsersController@login');
$router->get('getAllUsers', 'UsersController@getAllUsers');
$router->get('getUserByIdAPI', 'UsersController@getUserByIdAPI');
$router->get('getUsersActives', 'UsersController@getUsersActives');

//! Noticias
$router->post('createNew', 'NewsController@createNew');
$router->post('updateNew', 'NewsController@updateNew');
$router->post('deleteNew', 'NewsController@deleteNew');
$router->post('publishNew', 'NewsController@publishNew');
$router->post('unpublishNew', 'NewsController@unpublishNew');
$router->get('getByLanguage', 'NewsController@getByLanguage');
$router->get('getByLanguagePublic', 'NewsController@getByLanguagePublic');
$router->get('getNewsById', 'NewsController@getNewsById');
$router->get('getAllNews', 'NewsController@getAllNews');
$router->get('getNewsPublic', 'NewsController@getNewsPublic');
$router->get('getAllNewsLimit', 'NewsController@getAllNewsLimit');

//! Departamentos
$router->get('getDepartments', 'DepartmentsController@getDepartments');

//! Areas
$router->get('getAreasActives', 'AreasController@getAreasActives');

//! Administracion
$router->get('getAllAdministration', 'AdministrationController@getAllAdministration');

//! Roles
$router->get('getRolesActives', 'RolesController@getRolesActives');

//! Transparencia
$router->get('getAllTransparency', 'TransparencyController@getAllTransparency');
$router->get('getTransparencyById', 'TransparencyController@getTransparencyById');

//! Fracciones
$router->post('createFraction', 'FractionController@createFraction');
$router->post('updateFraction', 'FractionController@updateFraction');
$router->post('deleteFraction', 'FractionController@deleteFraction');
$router->get('getAllFraction', 'FractionController@getAllFraction');
$router->get('getFractionById', 'FractionController@getFractionById');
$router->get('getAllByAdmin', 'FractionController@getAllByAdmin');
$router->get('getAllByAdminandDepartment', 'FractionController@getAllByAdminandDepartment');
$router->get('getAllByAdminandtype', 'FractionController@getAllByAdminandtype');

//! Tipos de Fracciones
$router->get('getTypeFraction', 'TypeFractionController@getTypeFraction');

//! Periodos
$router->get('getAllPeriod', 'PeriodController@getAllPeriod');

//! secciones
$router->post('createSection', 'SectionController@createSection');
$router->post('updateSection', 'SectionController@updateSection');
$router->post('deleteSection', 'SectionController@deleteSection');
$router->get('getAllByFraction', 'SectionController@getAllByFraction');
$router->get('getAllBySubsection', 'SectionController@getAllBySubsection');
$router->get('getAllSection', 'SectionController@getAllSection');
$router->get('getSectionById', 'SectionController@getSectionById');
$router->get('getPeriodById', 'SectionController@getPeriodById');

//! Elementos
$router->post('createElement', 'ElementController@createElement');
$router->post('updateElement', 'ElementController@updateElement');
$router->post('deleteElement', 'ElementController@deleteElement');
$router->get('getAllElement', 'ElementController@getAllElement');
$router->get('getAllByPeriod', 'ElementController@getAllByPeriod');
$router->get('getElementById', 'ElementController@getElementById');
$router->get('getAllBySection', 'ElementController@getAllBySection');

