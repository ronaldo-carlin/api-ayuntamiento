<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    
    public $timestamps = false;
    protected $fillable = [
        'id',
        'title',
        'description',
        'cover_page',
        'multimedia',
        'video',
        'link',
        'link_youtube',
        'type',
        'department_id',
        'is_active',
        'deleted',
        'publish',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ];
}