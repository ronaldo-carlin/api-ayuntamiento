<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transparency extends Model
{
    protected $table = 'transparency';
    public $timestamps = false;
    protected $fillable = [
        'id',
        'fraction_id',
        'department',
        'administration',
        'description',
        'enlace',
        'filename',
        'is_active',
        'publish',
        'deleted',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ];
}