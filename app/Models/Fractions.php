<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fractions extends Model
{
    protected $table = 'fractions';
    public $timestamps = false;
    protected $fillable = [
        'id',
        'filename',
        'fraction',
        'name',
        'description',
        'format',
        'type_id',
        'department_id',
        'administration_id',
        'update_period',
        'update_date',
        'date_validation',
        'is_active',
        'publish',
        'deleted',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ];
}