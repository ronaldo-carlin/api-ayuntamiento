<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Areas extends Model
{
    
    public $timestamps = false;
    protected $fillable = [
        'id',
        'name',
        'location',
        'extent',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
        'deleted',
    ];
}