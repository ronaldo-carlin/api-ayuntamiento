<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'id',
        'name',
        'email',
        'password',
        'img',
        'date_birth',
        'phone',
        'address',
        'curp',
        'rfc',
        'imss',
        'date_admission',
        'administration_id',
        'department_id',
        'area_id',
        'rol_id',
        'secret_question_id',
        'secret_question_answered',
        'salary_day',
        'salary_month',
        'bonus_day',
        'is_active',
        'deleted',
        'updated_password_at',
        'created_at',
        'created_by',
        'update_at',
        'update_by',
        'deleted_at',
        'deleted_by',
        'position_id',
        'token',
    ];
}
