<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Elements extends Model
{
    protected $table = 'element';
    public $timestamps = false;
    protected $fillable = [
        'id',
        'section_id',
        'type_id',
        'link',
        'filename',
        'description',
        'path',
        'mime',
        'deleted',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ];
}