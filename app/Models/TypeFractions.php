<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeFractions extends Model
{
    protected $table = 'types_fraction';
    public $timestamps = false;
    protected $fillable = [
        'id',
        'type',
        'description',
    ];
}