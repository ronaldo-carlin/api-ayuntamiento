<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    
    protected $table = 'calendar';
    public $timestamps = false;
    protected $fillable = [
        'id',
        'name',
        'date_start',
        'date_end',
        'department_id',
        'created_at',
        'created_by',
        'update_at',
        'update_by',
        'deleted',
        'delete_at',
        'delete_by',
        'is_active',
        'event_id', 
        'repeat_yearly',
        'companies_avalibles',
    ];
}