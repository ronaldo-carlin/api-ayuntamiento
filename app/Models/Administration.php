<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Administration extends Model
{
    protected $table = 'administration';
    public $timestamps = false;
    protected $fillable = [
        'id',
        'name',
        'date_start',
        'date_end',
        'deleted',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ];
}
