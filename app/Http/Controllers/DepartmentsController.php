<?php

namespace App\Http\Controllers;

use App\Models\Departments;
use Illuminate\Http\Request;

class DepartmentsController extends Controller
{
    /* trae todos los departamentos */
    public function getDepartments()
    {

        $response = ['success' => false, 'message' => 'No se encontraron departamentos activos'];

        $Departments = Departments::select(
            'departments.id',
            'departments.name',
            'departments.management',
            'departments.is_active',
        )
            ->get();

        if (count($Departments) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontraron departamentos activos';
        }
        $response['departments'] = $Departments;
        $response['count'] = count($Departments);

        return response()->json($response);
    }
}
