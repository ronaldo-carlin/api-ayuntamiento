<?php

namespace App\Http\Controllers;

use App\Models\Elements;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ElementController extends Controller
{

    /* crea las elementos */
    public function createElement(Request $req)
    {

        try {
            Elements::create([
                'section_id' => $req->section_id,
                'link' => $req->link,
                'description' => $req->description,
                'created_at' => Carbon::now(),
                'created_by' =>   $req->created_by,
            ]);
            $response = ['success' => true, 'message' => 'Se creo el elemento'];
        } catch (\Throwable $e) {
            $response = ['success' => false, 'message' => $e];
            $response['message'] = "No se creo el elemento";
        }
        return $response;
    }

    /* modificar elemento */
    public function updateElement(Request $req)
    {

        try {
            Elements::where('id', $req->id)->update([
                'link' => $req->link,
                'description' => $req->description,
                'updated_at' => Carbon::now(),
                'updated_by' =>   $req->updated_by,

            ]);
            $response = ['success' => true, 'message' => 'Se actualizó el elemento'];
        } catch (\Throwable $e) {
            $response['success'] = false;
            $response['message'] = "No se actualizó el elemento correctamente";
            $response['error'] = $e->getMessage();
        }
        return $response;
    }

    /* borrar elemento */
    public function deleteElement(Request $req)
    {

        try {
            Elements::where('id', $req->id)->update([
                'deleted' => 1,
            ]);
            $response = ['success' => true, 'message' => 'Se elimino el elemento correctamente'];
        } catch (\Throwable $e) {
            $response['success'] = false;
            $response['message'] = "No se elimino correctamente";
        }
        return $response;
    }

    /* todos los elementos */
    public function getAllElement()
    {
        $response = ['success' => false, 'message' => 'No se encontraron elementos activos'];

        $Elements = Elements::select(
            'element.id',
            'element.section_id',
            'element.type_id',
            'element.link',
            'element.description',
            'element.deleted',
            'element.created_at',
            'element.created_by',

        )
            ->get();

        if (count($Elements) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontraron elementos activos';
        }
        $response['elements'] = $Elements;
        $response['count'] = count($Elements);

        return response()->json($response);
    }

    /* elemento por id */
    public function getElementById(Request $req)
    {
        $response = ['success' => false, 'message' => 'No se encontro el elemento '];

        $Elements = Elements::select(
            'id',
            'section_id',
            'type_id',
            'link',
            'description',
            'deleted',
            'created_at',
            'created_by',

        )
            ->where('id', $req->id)
            ->where('deleted', '=', 0)
            ->get();



        if (count($Elements) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontro el elemento';
        }
        $response['elements'] = $Elements;
        $response['count'] = count($Elements);

        return response()->json($response);
    }

    /* elemento del periodo */
    public function getAllByPeriod(Request $req)
    {
        $response = ['success' => false, 'message' => 'No se encontro el elemento '];

        $Elements = Elements::select(
            'id',
            'section_id',
            'type_id',
            'link',
            'description',
            'deleted',
            'created_at',
            'created_by',

        )
            ->where('section_id', $req->section_id)
            ->where('deleted', '=', 0)
            ->get();



        if (count($Elements) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontro el elemento';
        }
        $response['elements'] = $Elements;
        $response['count'] = count($Elements);

        return response()->json($response);
    }
}
