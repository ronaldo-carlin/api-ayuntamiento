<?php

namespace App\Http\Controllers;

use App\Models\TypeFractions;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TypeFractionController extends Controller
{

    /* todos los tipos de fracciones */
    public function getTypeFraction()
    {
        $response = ['success' => false, 'message' => 'No se encontraron tipo de fracciones activas'];

        $fractions = TypeFractions::select(
            'types_fraction.id',
            'types_fraction.type',
            'types_fraction.description',
        )
            ->get();

        if (count($fractions) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontraron todos los tipos de fracciones activas';
        }
        $response['TypeFractions'] = $fractions;
        $response['count'] = count($fractions);

        return response()->json($response);
    }
}
