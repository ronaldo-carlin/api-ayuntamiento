<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Carbon;

class UsersController extends Controller
{
    public function login(Request $req)
    {
        // $pass = sha1(md5($req->password));

        try {
            $user = User::select(
                'users.id',
                'users.name',
                'users.rol_id',
                'users.administration_id',
                'users.department_id',
                'users.updated_password_at',
            )
                ->where('users.password', '=', $req->password)
                ->where('users.email', '=', $req->email)
                ->where('users.deleted', '=', 0)
                ->get();

            if (count($user) > 0) {
                $response['success'] =  true;
                $response['message'] = 'Usuario encontrado';
                $response['user'] = $user;
            } else {
                $response['success'] =  false;
                $response['message'] = 'Usuario no  encontrado';
            }
        } catch (\Throwable $e) {
            $response = ['success' => false, 'message' => 'error'];
        }
        return $response;
    }

    public function createUser(Request $req)
    {
        if ($req->role_id == 1) {
        try {
            User::create([
                'name' => $req->name,
                'email' => $req->email,
                'password' => sha1(md5($req->password)),
                'img' => $req->img,
                'date_birth' =>   Carbon::parse($req->date_birth),
                'phone' => $req->phone,
                'address' => $req->address,
                'curp' => $req->curp,
                'rfc' => $req->rfc,
                'imss' => $req->imss,
                'date_admission' =>   Carbon::parse($req->date_admission),
                'department_id' => $req->department_id,
                'area_id' => $req->area_id,
                'rol_id' => $req->role,
                'administration_id' => $req->administration_id,
                'created_at' => Carbon::now(),
                'created_by' =>   $req->created_by,
            ]);
            $response['success'] = true;
            $response['message'] = "Se creo el usuario";
        } catch (\Throwable $e) {
            $response['success'] = false;
            $response['message'] = "No se creó el usuario correctamente" . $e->getMessage();
        }
        } else {
            $response['success'] = false;
            $response['message'] = "No se creó el usuario correctamente";
        }


        return $response;
    }

    public function updateUser(Request $req)
    {

        try {
            User::where('id', $req->id)->update([
                'name' => $req->name,
                'email' => $req->email,
                'img' => $req->img,
                'date_birth' =>   Carbon::parse($req->date_birth),
                'phone' => $req->phone,
                'address' => $req->address,
                'curp' => $req->curp,
                'rfc' => $req->rfc,
                'imss' => $req->imss,
                'date_admission' =>   Carbon::parse($req->date_admission),
                'department_id' => $req->department_id,
                'area_id' => $req->area_id,
                'rol_id' => $req->role,
                'update_at' => Carbon::now(),
                'update_by' => $req->update_by,
            ]);
            if ($req->img != '') {
                User::where('id', $req->id)->update([
                    'img' => $req->img,
                ]);
            }
            $response = ['success' => true, 'message' => 'Se actualizo el usuario'];
        } catch (\Throwable $e) {
            $response = ['success' => false, 'message' => 'No se actualizo: ' . $e->getMessage()];
        }
        return $response;
    }

    public function deleteUser(Request $req)
    {
        if ($req->role_id == 1) {
            try {
                User::where('id', $req->id)->update([
                    'deleted' => 1,
                    'is_active' => 0,
                    'deleted_at' => Carbon::now(),
                    'deleted_by' => $req->user,
                ]);
                $response = ['success' => true, 'message' => 'Se elimino correctamente'];
            } catch (\Throwable $e) {
                $response['success'] = false;
                $response['message'] = "No se elimino correctamente";
            }
        } else {
            $response['success'] = false;
            $response['message'] = "No se elimino correctamente";
        }

        return $response;
    }

    public static function forgotpassword(Request $req)
    {
        $token1 = bin2hex(random_bytes(8));
        $token2 = bin2hex(random_bytes(8));
        try {
            $user = User::select(
                'users.id',
                'users.email'
            )
                ->where('users.email', '=', $req->email)
                ->get();
            //* Pruebas
            //$link = 'http://localhost/comunidad/changepass.php?id=' . $token1 . $user[0]->id . $token2;
            //* Prod
            $link = 'https://comunidadavalanz.com.mx/changepass.php?id=' . $token1 . $user[0]->id . $token2;
            $text             = 'Ingresa a esta liga para poder recuperar tu contraseña: ' . $link;
            if (count($user) > 0) {
                $mail             = new PHPMailer(); // create a n
                $mail->CharSet = 'UTF-8';
                $mail->SMTPDebug  = 1; // debugging: 1 = errors and messages, 2 = messages only
                $mail->SMTPAuth   = true; // authentication enabled
                $mail->SMTPSecure = 'tls';
                $mail->isSMTP();
                $mail->Host = 'smtp.live.com';
                $mail->Port = 587;
                $mail->IsHTML(true);
                $mail->Username = 'comunidad@avalanz.com';
                $mail->Password = 'C0munid4d+.';
                $mail->SetFrom('comunidad@avalanz.com', 'Comunidad Avalanz');
                $mail->Subject = "Recuperar Contraseña";
                $mail->Body    = $text;

                $mail->AddAddress($user[0]->email, $user[0]->name);
                if ($mail->Send()) {
                    $send = true;
                } else {
                    $send =  false;
                }
                $response['success'] =  true;
                $response['message'] = 'Email encontrado';
                /*   $response['send'] =  $send; */
            } else {
                $response['success'] =  false;
                $response['message'] = 'El email no existe';
            }
        } catch (\Throwable $e) {
            $response = ['success' => false, 'message' => $e->getMessage()];
        }
        return  $response;
    }

    /* todos los usuarios */
    public function getAllUsers()
    {
        $response = ['success' => false, 'message' => 'No se encontraron usuarios activos'];

        $users = User::select(
            'users.id',
            'users.name',
            'users.email',
            'users.password',
            'users.img',
            'users.date_birth',
            'users.phone',
            'users.address',
            'users.curp',
            'users.rfc',
            'users.imss',
            'users.date_admission',
            'users.department_id',
            'users.area_id',
            'users.rol_id',
            'users.is_active',
        )
            ->where('users.deleted', '=', 0)
            ->get();

        if (count($users) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontraron usuarios activos';
        }
        $response['user'] = $users;
        $response['count'] = count($users);

        return response()->json($response);
    }

    /* todos los usuarios activos */
    public function getUsersActives(Request $req)
    {
        $response = ['success' => false, 'message' => 'No se encotraron usuarios '];

        if ($req->role_id == 1) {

            $users = User::select(
                'users.id',
                'users.name',
                'users.email',
                'users.password',
                'users.date_birth',
                'users.phone',
                'users.rfc',
                'users.curp',
                'users.imss',
                'users.area_id',
                'users.rol_id',
                'users.department_id',
                'users.img',
                'users.is_active',
                'c.name as departments',
            )
                /* ->whereNotIn('users.id', [1, 464,  489, 890, 605, 931]) */
                /* ->join('company as c', 'users.company_id', '=', 'c.id') */
                ->leftjoin('departments as c', 'users.department_id', '=', 'c.id')
                ->where('users.deleted', '=', 0)
                ->get();
            //$releases = Release::where('deleted', 0)->get();

            if (count($users) > 0) {
                $response['success'] =  true;
                $response['message'] = 'Se encontraron usuarios activos';
            }
            $response['users'] = $users;
            $response['count'] = count($users);
        }
        return response()->json($response);
    }

    /* usuarios por id */
    public function getUserByIdAPI(Request $req)
    {
        $response = ['success' => false, 'message' => 'No se encontraron usuarios activos'];

        $users = User::select(
            'users.id',
            'users.name',
            'users.email',
            'users.password',
            'users.img',
            'users.date_birth',
            'users.phone',
            'users.address',
            'users.curp',
            'users.rfc',
            'users.imss',
            'users.date_admission',
            'users.department_id',
            'users.area_id',
            'users.rol_id',
            'd.name as departments',
        )
            ->leftjoin('departments as d', 'users.department_id', '=', 'd.id')
            ->where('users.id', $req->id)
            ->get();

        if (count($users) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontraron usuarios activos';
        }
        $response['user'] = $users;
        $response['count'] = count($users);

        return response()->json($response);
    }

    /* actualizar contraseña */
    public function updatePassword(Request $req)
    {
        if ($req->first == 1) {
            try {
                User::where('id', $req->id)->update([
                    'password' => $req->password,
                    /* 'img' => $req->img, */
                    'updated_password_at' => Carbon::now(),
                ]);
                $response = ['success' => true, 'message' => 'Se actualizo el password correctamente'];
            } catch (\Throwable $e) {
                $response['success'] = false;
                $response['message'] = "No se actualizo el password correctamente";
            }
        } else {
            try {
                User::where('id', $req->id)->update([
                    'password' => $req->password
                ]);
                $response = ['success' => true, 'message' => 'Se actualizo el password correctamente'];
            } catch (\Throwable $e) {
                $response['success'] = false;
                $response['message'] = "No se actualizo el password correctamente";
            }
        }

        return $response;
    }

    /* pendiente */
    public function getPermissonsByUserAndModuleApi(Request $req)
    {
        $response = ['success' => false, 'message' => 'No se encontraron usuarios activos'];

        $users = User::select(
            'u.rol_id as users',
            'p.name as departments',
            'p.name as departments',
        )
            ->innerjoin('permissions as p', 'users.company_id', '=', 'c.id')
            ->where('users.deleted', '=', 0)
            /* ->where('users.id' $req->name ) */
            ->get();

        if (count($users) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontraron usuarios activos';
        }
        $response['user'] = $users;
        $response['count'] = count($users);

        return response()->json($response);
    }
}
