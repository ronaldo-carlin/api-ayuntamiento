<?php

namespace App\Http\Controllers;

use App\Models\Areas;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AreasController extends Controller
{

    /* apis por post */

    /* crear area */
    public function createArea(Request $req)
    {

        try {
            Areas::create([
                'name' => $req->name,
                'created_at' => Carbon::now(),
                'created_by' =>   $req->created_by,
            ]);
            $response = ['success' => true, 'message' => 'Se creo el nuevo rol'];
        } catch (\Throwable $e) {
            $response = ['success' => false, 'message' => $e];
            $response['message'] = "No se creo el rol";
        }
        return $response;
    }

    /* areas que estan activos */
    public function getAreasActives(Request $req)
    {
        $response = ['success' => false, 'message' => 'No se encotraron areas '];

        $areas = Areas::select(
            'areas.id',
            'areas.name',
            'areas.created_at',
            'areas.created_by',
        )
            ->where('areas.deleted', '=', 0)
            ->get();



        if (count($areas) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontraron las areas';
        }
        $response['areas'] = $areas;
        $response['count'] = count($areas);

        return response()->json($response);
    }
}
