<?php

namespace App\Http\Controllers;

use App\Models\Fractions;
use Carbon\Carbon;
use Illuminate\Http\Request;

class FractionController extends Controller
{

    /* crea las fracciones */
    public function createFraction(Request $req)
    {

        try {
            Fractions::create([
                'filename' => $req->filename,
                'fraction' => $req->fraction,
                'name' => $req->name,
                'description' => $req->description,
                'format' => $req->format,
                'update_date' => Carbon::parse($req->update_date),
                'date_validation' =>   Carbon::parse($req->date_validation),
                'type_id' => $req->type_id,
                'department_id' => $req->department_id,
                'administration_id' => $req->administration_id,
                'update_period' => $req->update_period,
                'created_at' => Carbon::now(),
                'created_by' =>   $req->created_by,
            ]);
            $response = ['success' => true, 'message' => 'Se creo la Fraccion'];
        } catch (\Throwable $e) {
            $response = ['success' => false, 'message' => $e];
            $response['message'] = "No se creo la Fracción";
        }
        return $response;
    }

    /* modificar fraccion */ /* listo */
    public function updateFraction(Request $req)
    {
        //! Cuando filename viene datos
        if ($req->filename != '') {
            try {
                Fractions::where('id', $req->id)->update([
                    'filename' => $req->filename,
                    'fraction' => $req->fraction,
                    'name' => $req->name,
                    'description' => $req->description,
                    'format' => $req->format,
                    'update_date' => Carbon::parse($req->update_date),
                    'date_validation' =>   Carbon::parse($req->date_validation),
                    'type_id' => $req->type_id,
                    'department_id' => $req->department_id,
                    'update_period' => $req->update_period,
                    'updated_at' => Carbon::now(),
                    'updated_by' =>   $req->updated_by,

                ]);
                $response = ['success' => true, 'message' => 'Se acualizo la fraccion con el icono'];
            } catch (\Throwable $e) {
                $response['success'] = false;
                $response['message'] = "No se actualizo la fraccion correctamente";
                $response['error'] = $e->getMessage();
            }
            return $response;
        }
        //! Todos filename viene vacio
        else {
            try {
                Fractions::where('id', $req->id)->update([
                    'fraction' => $req->fraction,
                    'name' =>  $req->name,
                    'description' => $req->description,
                    'format' => $req->format,
                    'update_date' => Carbon::parse($req->update_date),
                    'date_validation' =>   Carbon::parse($req->date_validation),
                    'type_id' => $req->type_id,
                    'department_id' => $req->department_id,
                    'update_period' => $req->update_period,
                    'updated_at' => Carbon::now(),
                    'updated_by' =>   $req->updated_by,
                ]);
                $response = ['success' => true, 'message' => 'Se acualizo la fraccion sin el icono'];
            } catch (\Throwable $e) {
                $response['success'] = false;
                $response['message'] = "No se actualizo la fraccion correctamente";
                $response['error'] = $e->getMessage();
            }
            return $response;
        }
    }

    /* borrar fraccion */ /* listo */
    public function deleteFraction(Request $req)
    {

        try {
            Fractions::where('id', $req->id)->update([
                'deleted' => 1,
            ]);
            $response = ['success' => true, 'message' => 'Se elimino correctamente'];
        } catch (\Throwable $e) {
            $response['success'] = false;
            $response['message'] = "No se elimino correctamente";
        }
        return $response;
    }

    /* todas las fracciones */
    public function getAllFraction()
    {
        $response = ['success' => false, 'message' => 'No se encontraron fracciones activas'];

        $fractions = Fractions::select(
            'fractions.id',
            'fractions.filename',
            'fractions.fraction',
            'fractions.name',
            'fractions.description',
            'fractions.type_id',
            't.type as type',
            'fractions.department_id',
            'd.name as departments',
            'a.name as administration',
            'fractions.update_period',
            'fractions.date_validation',
            'fractions.is_active',
            'fractions.publish',
            'fractions.deleted',
            'fractions.created_at',
            'fractions.created_by',

        )
            ->leftjoin('departments as d', 'fractions.department_id', '=', 'd.id')
            ->leftjoin('administration as a', 'fractions.administration_id', '=', 'a.id')
            ->leftjoin('types_fraction as t', 'fractions.type_id', '=', 't.id')
            ->where('fractions.deleted', '=', 0)
            ->get();

        if (count($fractions) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontraron fracciones activas';
        }
        $response['fractions'] = $fractions;
        $response['count'] = count($fractions);

        return response()->json($response);
    }

    /* todas las fracciones del la administracion especifica */
    public function getAllByAdmin(Request $req)
    {
        $response = ['success' => false, 'message' => 'No se encontraron fracciones activas'];

        $fractions = Fractions::select(
            'fractions.id',
            'fractions.filename',
            'fractions.fraction',
            'fractions.name',
            'fractions.description',
            't.type as type',
            'fractions.department_id',
            'd.name as departments',
            'a.name as administration',
            'fractions.update_period',
            'fractions.date_validation',
            'fractions.is_active',
            'fractions.publish',
            'fractions.deleted',
            'fractions.created_at',
            'fractions.created_by',

        )
            ->leftjoin('departments as d', 'fractions.department_id', '=', 'd.id')
            ->leftjoin('administration as a', 'fractions.administration_id', '=', 'a.id')
            ->leftjoin('types_fraction as t', 'fractions.type_id', '=', 't.id')
            ->where('fractions.deleted', '=', 0)
            ->where('fractions.administration_id', '=', $req->admin)
            ->get();

        if (count($fractions) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontraron fracciones activas';
        }
        $response['fractions'] = $fractions;
        $response['count'] = count($fractions);

        return response()->json($response);
    }

    /* todas las fracciones del la administracion especifica */
    public function getAllByAdminandDepartment(Request $req)
    {
        $response = ['success' => false, 'message' => 'No se encontraron fracciones activas'];

        $fractions = Fractions::select(
            'fractions.id',
            'fractions.filename',
            'fractions.fraction',
            'fractions.name',
            'fractions.description',
            't.type as type',
            'fractions.department_id',
            'd.name as departments',
            'a.name as administration',
            'fractions.update_period',
            'fractions.date_validation',
            'fractions.is_active',
            'fractions.publish',
            'fractions.deleted',
            'fractions.created_at',
            'fractions.created_by',

        )
            ->leftjoin('departments as d', 'fractions.department_id', '=', 'd.id')
            ->leftjoin('administration as a', 'fractions.administration_id', '=', 'a.id')
            ->leftjoin('types_fraction as t', 'fractions.type_id', '=', 't.id')
            ->where('fractions.deleted', '=', 0)
            ->where('fractions.department_id', '=', $req->department_id)
            ->where('fractions.administration_id', '=', $req->admin)
            ->get();

        if (count($fractions) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontraron fracciones activas';
        }
        $response['fractions'] = $fractions;
        $response['count'] = count($fractions);

        return response()->json($response);
    }

    /* todas las fracciones del la administracion y tipo especifica */
    public function getAllByAdminandtype(Request $req)
    {
        $response = ['success' => false, 'message' => 'No se encontraron fracciones activas'];

        $fractions = Fractions::select(
            'fractions.id',
            'fractions.filename',
            'fractions.fraction',
            'fractions.name',
            'fractions.description',
            't.type as type',
            'fractions.department_id',
            'd.name as departments',
            'a.name as administration',
            'fractions.update_period',
            'fractions.update_date',
            'fractions.date_validation',
            'fractions.is_active',
            'fractions.publish',
            'fractions.deleted',
            'fractions.created_at',
            'fractions.created_by',

        )
            ->leftjoin('departments as d', 'fractions.department_id', '=', 'd.id')
            ->leftjoin('administration as a', 'fractions.administration_id', '=', 'a.id')
            ->leftjoin('types_fraction as t', 'fractions.type_id', '=', 't.id')
            ->where('fractions.deleted', '=', 0)
            ->where('fractions.administration_id', '=', $req->admin)
            ->where('fractions.type_id', '=', $req->type)
            ->get();

        if (count($fractions) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontraron fracciones activas';
        }
        $response['fractions'] = $fractions;
        $response['count'] = count($fractions);

        return response()->json($response);
    }

    /* fraccion por id */
    public function getFractionById(Request $req)
    {
        $response = ['success' => false, 'message' => 'No se encontro la fraccion '];

        $fractions = Fractions::select(
            'fractions.id',
            'fractions.filename',
            'fractions.fraction',
            'fractions.name',
            'fractions.description',
            'fractions.format',
            'fractions.type_id',
            't.type as type',
            'fractions.department_id',
            'd.name as departments',
            'a.name as administration',
            's.id as section_id',
            'fractions.update_period',
            'p.name as period',
            'fractions.update_date',
            'fractions.date_validation',
            'fractions.is_active',
            'fractions.publish',
            'fractions.deleted',
            'fractions.created_at',
            'fractions.created_by',

        )
            ->leftjoin('departments as d', 'fractions.department_id', '=', 'd.id')
            ->leftjoin('administration as a', 'fractions.administration_id', '=', 'a.id')
            ->leftjoin('types_fraction as t', 'fractions.type_id', '=', 't.id')
            ->leftjoin('sections as s', 'fractions.id', '=', 's.fraction_id')
            ->leftjoin('period as p', 'fractions.update_period', '=', 'p.id')
            ->where('fractions.id', $req->id)
            ->where('fractions.deleted', '=', 0)
            ->get();



        if (count($fractions) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontro la fraccion';
        }
        $response['fraction'] = $fractions;
        $response['count'] = count($fractions);

        return response()->json($response);
    }
}
