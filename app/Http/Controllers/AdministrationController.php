<?php

namespace App\Http\Controllers;

use App\Models\Administration;
use Illuminate\Http\Request;

class AdministrationController extends Controller
{
    /* trae todos las administraciones */
    public function getAllAdministration()
    {

        $response = ['success' => false, 'message' => 'No se encontraron administraciones disponibles'];

        $Administration = Administration::select(
            'administration.id',
            'administration.name',
            'administration.date_start',
            'administration.date_end',
        )
            ->where('administration.deleted', '=', 0)
            ->get();

        if (count($Administration) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontraron administraciones activos';
        }
        $response['administration'] = $Administration;
        $response['count'] = count($Administration);

        return response()->json($response);
    }
}
