<?php

namespace App\Http\Controllers;

use App\Models\Roles;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RolesController extends Controller
{

    /* apis por post */

    /* crear role */
    public function createRol(Request $req)
    {

        try {
            Roles::create([
                'name' => $req->name,
                'is_active' => $req->is_active,
                'created_at' => Carbon::now(),
                'created_by' =>   $req->created_by,
            ]);
            $response = ['success' => true, 'message' => 'Se creo el nuevo rol'];
        } catch (\Throwable $e) {
            $response = ['success' => false, 'message' => $e];
            $response['message'] = "No se creo el rol";
        }
        return $response;
    }

    /* roles que estan activos */
    public function getRolesActives(Request $req)
    {
        $response = ['success' => false, 'message' => 'No se encotraron roles '];

        $roles = Roles::select(
            'roles.id',
            'roles.name',
            'roles.is_active',
            'roles.created_at',
            'roles.created_by',
        )
            ->where('roles.deleted', '=', 0)
            ->get();



        if (count($roles) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontraron los roles';
        }
        $response['roles'] = $roles;
        $response['count'] = count($roles);

        return response()->json($response);
    }
}
