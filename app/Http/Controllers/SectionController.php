<?php

namespace App\Http\Controllers;

use App\Models\Sections;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SectionController extends Controller
{

    /* crea las secciones */
    public function createSection(Request $req)
    {

        if ($req->type == 1) {
            try {
                Sections::create([
                    'fraction_id' => $req->fraction_id,
                    'name' => $req->name,
                    'description' => $req->description,
                    'created_at' => Carbon::now(),
                    'created_by' =>   $req->created_by,
                ]);
                $response = ['success' => true, 'message' => 'Se creo la Seccion'];
            } catch (\Throwable $e) {
                $response = ['success' => false, 'message' => $e];
                $response['message'] = "No se creo la Fracción";
            }
            return $response;
        } else {
            try {
                Sections::create([
                    'subsection' => $req->subsection,
                    'name' => $req->name,
                    'description' => $req->description,
                    'created_at' => Carbon::now(),
                    'created_by' =>   $req->created_by,
                ]);
                $response = ['success' => true, 'message' => 'Se creo la Periodo'];
            } catch (\Throwable $e) {
                $response = ['success' => false, 'message' => $e];
                $response['message'] = "No se creo la Fracción";
            }
            return $response;
        }
    }

    /* modificar fraccion */
    public function updateSection(Request $req)
    {

        try {
            Sections::where('id', $req->id)->update([
                'name' => $req->name,
                'description' => $req->description,
                'updated_at' => Carbon::now(),
                'updated_by' =>   $req->updated_by,

            ]);
            $response = ['success' => true, 'message' => 'Se actualizó la sección de la fracción'];
        } catch (\Throwable $e) {
            $response['success'] = false;
            $response['message'] = "No se actualizó la sección correctamente";
            $response['error'] = $e->getMessage();
        }
        return $response;
    }

    /* borrar fraccion */
    public function deleteSection(Request $req)
    {

        try {
            Sections::where('id', $req->id)->update([
                'deleted' => 1,
            ]);
            $response = ['success' => true, 'message' => 'Se elimino correctamente'];
        } catch (\Throwable $e) {
            $response['success'] = false;
            $response['message'] = "No se elimino correctamente";
        }
        return $response;
    }

    /* todas las secciones */
    public function getAllSection()
    {
        $response = ['success' => false, 'message' => 'No se encontraron secciones activas'];

        $sections = Sections::select(
            'sections.id',
            'sections.name',
            'sections.description',
            'sections.fraction_id',
            'sections.subsection',

            'sections.created_at',
            'sections.created_by',

        )
            ->get();

        if (count($sections) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontraron secciones activas';
        }
        $response['sections'] = $sections;
        $response['count'] = count($sections);

        return response()->json($response);
    }

    /* todas las secciones del la fraccion especifica */
    public function getAllByFraction(Request $req)
    {
        $response = ['success' => false, 'message' => 'No se encontraron secciones activas'];

        $sections = Sections::select(
            'sections.id',
            'sections.name',
            'sections.description',
            'sections.fraction_id',

        )
            ->where('sections.fraction_id', '=', $req->fraction)
            ->where('sections.deleted', '=', 0)
            ->orderby('sections.name')
            ->get();

        if (count($sections) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontraron secciones activas';
        }
        $response['sections'] = $sections;
        $response['count'] = count($sections);

        return response()->json($response);
    }

    /* todas las periodos del la seccion especifica */
    public function getAllBySubsection(Request $req)
    {
        $response = ['success' => false, 'message' => 'No se encontraron secciones activas'];

        $sections = Sections::select(
            'sections.id',
            'sections.name',
            'sections.description',

            'sections.fraction_id',
            'sections.subsection',

        )
            ->where('sections.subsection', '=', $req->section)
            ->where('sections.deleted', '=', 0)
            ->orderby('sections.name')
            ->get();

        if (count($sections) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontraron secciones activas';
        }
        $response['sections'] = $sections;
        $response['count'] = count($sections);

        return response()->json($response);
    }

    /* fraccion por id */
    public function getSectionById(Request $req)
    {
        $response = ['success' => false, 'message' => 'No se encontro la fraccion '];

        $sections = Sections::select(
            'sections.id',
            'sections.name',
            'sections.description',
            'sections.fraction_id',
            'sections.subsection',

            'sections.created_at',
            'sections.created_by',

        )
            ->where('sections.id', $req->id)
            ->where('sections.deleted', '=', 0)
            ->get();



        if (count($sections) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontro la fraccion';
        }
        $response['section'] = $sections;
        $response['count'] = count($sections);

        return response()->json($response);
    }

    /* periodo por id */
    public function getPeriodById(Request $req)
    {
        $response = ['success' => false, 'message' => 'No se encontro la fraccion '];

        $sections = Sections::select(
            'sections.id',
            'sections.name',
            'sections.description',
            'sections.fraction_id',
            'sections.subsection',
        )
            ->where('sections.id', $req->id)
            ->where('sections.deleted', '=', 0)
            ->get();



        if (count($sections) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontro la fraccion';
        }
        $response['section'] = $sections;
        $response['count'] = count($sections);

        return response()->json($response);
    }
}
