<?php

namespace App\Http\Controllers;

use App\Models\Transparency;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TransparencyController extends Controller
{

    /* todas las transparencias */
    public function getAllTransparency()
    {
        $response = ['success' => false, 'message' => 'No se encontraron transparencias activas'];

        $transparency = Transparency::select(
            'transparency.id',
            'f.fraction as fraction',
            'f.description as description',
            /* 'transparency.description', */
            'd.name as departments',
            'a.name as administration',
            't.type as type',
            'transparency.enlace',
            'transparency.filename',
            'transparency.is_active',
            'transparency.publish',
            'transparency.deleted',
            'transparency.created_at',
            'transparency.created_by',
            'transparency.created_by',
            'transparency.updated_at',
        )
            ->leftjoin('departments as d', 'transparency.department_id', '=', 'd.id')
            ->leftjoin('administration as a', 'transparency.administration_id', '=', 'a.id')
            ->leftjoin('fractions as f', 'transparency.fraction_id', '=', 'f.id')
            ->leftjoin('types_fraction as t', 'transparency.type_id', '=', 't.id')
            ->where('transparency.deleted', '=', 0)
            /* ->orderBy('created_at', 'DESC') */
            ->get();

        if (count($transparency) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontraron transparencias activas';
        }
        $response['transparency'] = $transparency;
        $response['count'] = count($transparency);

        return response()->json($response);
    }

    /* transparencias por id */
    public function getTransparencyById(Request $req)
    {
        $response = ['success' => false, 'message' => 'No se encontro la transparencia '];

        $transparency = Transparency::select(
            'transparency.id',
            'f.fraction as fraction',
            'f.description as description',
            'd.name as departments',
            'a.name as administration',
            't.type as type',
            'transparency.enlace',
            'transparency.filename',
            'transparency.is_active',
            'transparency.publish',
            'transparency.deleted',
            'transparency.created_at',
            'transparency.created_by',
            'transparency.created_by',
            'transparency.updated_at',
        )
            ->leftjoin('departments as d', 'transparency.department_id', '=', 'd.id')
            ->leftjoin('administration as a', 'transparency.administration_id', '=', 'a.id')
            ->leftjoin('fractions as f', 'transparency.fraction_id', '=', 'f.id')
            ->leftjoin('types_fraction as t', 'transparency.type_id', '=', 't.id')
            ->where('transparency.id', $req->id)
            ->where('transparency.deleted', '=', 0)
            ->get();



        if (count($transparency) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontro la transparencia';
        }
        $response['transparency'] = $transparency;
        $response['count'] = count($transparency);

        return response()->json($response);
    }

    /* crear transparencia */ /* listo */
    public function createNew(Request $req)
    {

        try {
            News::create([
                'fraction_id' => $req->fraction_id,
                'department' => $req->department,
                'filename' => $req->filename,
                'administration' => $req->administration,
                'description' => $req->description,
                'enlace' => $req->enlace,
                'link_youtube' => $req->link_youtube,
                'department_id' => $req->departments,
                'type' => $req->type,
                'publish' => $req->publish,
                'date_validity' =>   Carbon::parse($req->date_validity),
                'created_at' => Carbon::now(),
                'created_by' =>   $req->created_by,
            ]);
            $response = ['success' => true, 'message' => 'Se creo la transparencia'];
        } catch (\Throwable $e) {
            $response = ['success' => false, 'message' => $e];
            $response['message'] = "No se creo la transparencia";
        }
        return $response;
    }

    /* modificar transparencia */ /* listo */
    public function updateNew(Request $req)
    {
        //! Cuando todos vienen vacíos
        if ($req->filename != '' && $req->administration != '' && $req->description != '') {
            try {
                News::where('id', $req->id)->update([
                    'fraction_id' => $req->fraction_id,
                    'department' => $req->department,
                    'filename' => $req->filename,
                    'administration' => $req->administration,
                    'description' => $req->description,
                    'enlace' => $req->enlace,
                    'link_youtube' => $req->link_youtube,
                    'department_id' => $req->departments,
                    'type' => $req->type,
                    'is_active' => $req->is_active,
                    'publish' => $req->publish,
                    'date_validity' =>  $req->date_validity,
                    /* 'update_at' => Carbon::now(),
                    'update_by' =>   $req->update_by, */

                ]);
                $response = ['success' => true, 'message' => 'Se acualizo la transparencia con description, imagen de portada y administration', 'lenguaje' => $req->type];
            } catch (\Throwable $e) {
                $response['success'] = false;
                $response['message'] = "No se actualizo la transparencia correctamente";
                $response['error'] = $e->getMessage();
            }
            return $response;
        }
        //! Cuando el description viene vacío
        else if ($req->filename != '' &&  $req->administration != '') {
            try {
                News::where('id', $req->id)->update([
                    'fraction_id' => $req->fraction_id,
                    'department' => $req->department,
                    'filename' => $req->filename,
                    'administration' => $req->administration,
                    'enlace' => $req->enlace,
                    'link_youtube' => $req->link_youtube,
                    'department_id' => $req->departments,
                    'type' => $req->type,
                    'is_active' => $req->is_active,
                    'publish' => $req->publish,
                    'date_validity' =>  $req->date_validity,
                    /* 'update_at' => Carbon::now(),
                    'update_by' =>   $req->update_by, */
                ]);
                $response = ['success' => true, 'message' => 'Se acualizo la transparencia con imagen de portada nueva y administration', 'lenguaje' => $req->type];
            } catch (\Throwable $e) {
                $response['success'] = false;
                $response['message'] = "No se actualizo la transparencia correctamente";
                $response['error'] = $e->getMessage();
            }
        }
        //! Cuando la administration viene vacía 
        else if ($req->filename != '' &&  $req->description != '') {
            try {
                News::where('id', $req->id)->update([
                    'fraction_id' => $req->fraction_id,
                    'department' => $req->department,
                    'filename' => $req->filename,
                    'description' => $req->description,
                    'enlace' => $req->enlace,
                    'link_youtube' => $req->link_youtube,
                    'department_id' => $req->departments,
                    'type' => $req->type,
                    'is_active' => $req->is_active,
                    'publish' => $req->publish,
                    'date_validity' =>  $req->date_validity,
                    /* 'update_at' => Carbon::now(),
                    'update_by' =>   $req->update_by, */
                ]);
                $response = ['success' => true, 'message' => 'Se acualizo la transparencia con imagen de portada nueva y description', 'lenguaje' => $req->type];
            } catch (\Throwable $e) {
                $response['success'] = false;
                $response['message'] = "No se actualizo la transparencia correctamente";
                $response['error'] = $e->getMessage();
            }
        }
        //! Cuando la portada viene vacía
        else if ($req->administration != '' &&  $req->description != '') {
            try {
                News::where('id', $req->id)->update([
                    'fraction_id' => $req->fraction_id,
                    'department' => $req->department,
                    'administration' => $req->administration,
                    'description' => $req->description,
                    'enlace' => $req->enlace,
                    'link_youtube' => $req->link_youtube,
                    'department_id' => $req->departments,
                    'type' => $req->type,
                    'is_active' => $req->is_active,
                    'publish' => $req->publish,
                    'date_validity' =>  $req->date_validity,
                    /* 'update_at' => Carbon::now(),
                    'update_by' =>   $req->update_by, */
                ]);
                $response = ['success' => true, 'message' => 'Se acualizo la transparencia con imagen de portada nueva y description', 'lenguaje' => $req->type];
            } catch (\Throwable $e) {
                $response['success'] = false;
                $response['message'] = "No se actualizo la transparencia correctamente";
                $response['error'] = $e->getMessage();
            }
        }
        //! Cuando description y administration vienen vacios
        else if ($req->filename != '') {
            try {
                News::where('id', $req->id)->update([
                    'fraction_id' => $req->fraction_id,
                    'department' => $req->department,
                    'filename' => $req->filename,
                    'enlace' => $req->enlace,
                    'link_youtube' => $req->link_youtube,
                    'department_id' => $req->departments,
                    'type' => $req->type,
                    'is_active' => $req->is_active,
                    'publish' => $req->publish,
                    'date_validity' =>  $req->date_validity,
                    /* 'update_at' => Carbon::now(),
                    'update_by' =>   $req->update_by, */
                ]);
                $response = ['success' => true, 'message' => 'Se acualizo la transparencia con imagen de portada nueva', 'lenguaje' => $req->type];
                $response = ['type' => $req->type];
            } catch (\Throwable $e) {
                $response['success'] = false;
                $response['message'] = "No se actualizo la transparencia correctamente";
                $response['error'] = $e->getMessage();
            }
        }
        //! Cuando description y portada vienen vacios
        else if ($req->administration != '') {
            try {
                News::where('id', $req->id)->update([
                    'fraction_id' => $req->fraction_id,
                    'department' => $req->department,
                    'administration' => $req->administration,
                    'enlace' => $req->enlace,
                    'link_youtube' => $req->link_youtube,
                    'department_id' => $req->departments,
                    'type' => $req->type,
                    'is_active' => $req->is_active,
                    'publish' => $req->publish,
                    'date_validity' =>  $req->date_validity,
                    /* 'update_at' => Carbon::now(),
                    'update_by' =>   $req->update_by, */
                ]);
                $response = ['success' => true, 'message' => 'Se acualizo la transparencia con administration nueva', 'lenguaje' => $req->type];
            } catch (\Throwable $e) {
                $response['success'] = false;
                $response['message'] = "No se actualizo la transparencia correctamente";
                $response['error'] = $e->getMessage();
            }
        }
        //! Cuando administration y portada vienen vacios
        else if ($req->description != '') {
            try {
                News::where('id', $req->id)->update([
                    'fraction_id' => $req->fraction_id,
                    'department' => $req->department,
                    'description' => $req->description,
                    'enlace' => $req->enlace,
                    'link_youtube' => $req->link_youtube,
                    'department_id' => $req->departments,
                    'type' => $req->type,
                    'is_active' => $req->is_active,
                    'publish' => $req->publish,
                    'date_validity' =>  $req->date_validity,
                    /* 'update_at' => Carbon::now(),
                    'update_by' =>   $req->update_by, */
                ]);
                $response = ['success' => true, 'message' => 'Se acualizo la transparencia con description nuevo', 'lenguaje' => $req->type];
            } catch (\Throwable $e) {
                $response['success'] = false;
                $response['message'] = "No se actualizo la transparencia correctamente";
                $response['error'] = $e->getMessage();
            }
        }
        //! Todos vacios
        else {
            try {
                News::where('id', $req->id)->update([
                    'fraction_id' => $req->fraction_id,
                    'department' => $req->department,
                    'enlace' => $req->enlace,
                    'link_youtube' => $req->link_youtube,
                    'department_id' => $req->departments,
                    'type' => $req->type,
                    'is_active' => $req->is_active,
                    'publish' => $req->publish,
                    'date_validity' =>  $req->date_validity,
                    /* 'update_at' => Carbon::now(),
                    'update_by' =>   $req->update_by, */
                ]);
                $response = ['success' => true, 'message' => 'Se acualizo el comunicado sin description, imagen de portada y administration nueva'];
            } catch (\Throwable $e) {
                $response['success'] = false;
                $response['message'] = "No se actualizo la transparencia correctamente";
                $response['error'] = $e->getMessage();
            }
            return $response;
        }
    }

    /* borrar transparencia */ /* listo */
    public function deleteNew(Request $req)
    {

        try {
            News::where('id', $req->id)->update([
                'deleted' => 1,
            ]);
            $response = ['success' => true, 'message' => 'Se elimino correctamente'];
        } catch (\Throwable $e) {
            $response['success'] = false;
            $response['message'] = "No se elimino correctamente";
        }
        return $response;
    }

    /* transparencias publicada */ /* listo */
    public function publishNew(Request $req)
    {
        $response = ['success' => true, 'message' => 'Se publico correctamente'];

        try {
            News::where('id', $req->id)->update([
                'publish' => 1,
            ]);
        } catch (\Throwable $e) {
            $response['success'] = false;
            $response['message'] = "No se publico correctamente";
        }
        return $response;
    }

    /* transparencias no publicadas */ /* listo */
    public function unpublishNew(Request $req)
    {
        $response = ['success' => true, 'message' => 'Se despublico correctamente'];

        try {
            News::where('id', $req->id)->update([
                'publish' => 0,
            ]);
        } catch (\Throwable $e) {
            $response['success'] = false;
            $response['message'] = "No se despublico correctamente";
        }
        return $response;
    }

    /* apis por get */

    /* transparencias filtradas por lenguaje */
    public function getBytype(Request $req)
    {
        $response = ['success' => false, 'message' => 'No se encontraron transparencias activos'];

        //! Español
        if ($req->type == 1) {
            $transparency = Transparency::select(
                'transparency.id',
                'transparency.fraction_id',
                'transparency.department',
                'transparency.filename',
                'transparency.administration',
                'transparency.publish',
                'transparency.created_at',
                'transparency.created_by',
            )
                ->where('transparency.deleted', '=', 0)
                ->where('transparency.type', '=', 1)
                ->get();
        }
        //! Ingles
        elseif ($req->type == 2) {
            $transparency = Transparency::select(
                'transparency.id',
                'transparency.is_active',
                'transparency.department',
                'transparency.filename',
                'transparency.administration',
                'transparency.publish',
                'transparency.created_at',
                'transparency.created_by',

            )
                ->where('transparency.deleted', '=', 0)
                ->where('transparency.type', '=', 2)
                ->get();
        }
        //! Ambas
        else {
            $transparency = Transparency::select(
                'transparency.id',
                'transparency.fraction_id',
                'transparency.department',
                'transparency.is_active',
                'transparency.filename',
                'transparency.administration',
                'transparency.publish',
                'transparency.type',
                'transparency.created_at',
                'transparency.created_by',
            )
                ->where('transparency.deleted', '=', 0)
                ->where('transparency.type', '=', 3)
                ->get();
        }

        if (count($transparency) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontraron transparencias activos';
        }
        $response['transparency'] = $transparency;
        $response['count'] = count($transparency);

        return response()->json($response);
    }

    /* transparencias publicadas por lenguaje */
    public function getBytypePublic(Request $req)
    {
        if ($req->publish == 1) {

            //! Español
            if ($req->type == 1) {
                $transparency = Transparency::select(
                    'transparency.id',
                    'transparency.fraction_id',
                    'transparency.department',
                    'transparency.filename',
                    'transparency.administration',
                    'transparency.publish',
                    'transparency.created_at',
                    'transparency.created_by',
                )
                    ->where('transparency.publish', '=', 1)
                    ->where('transparency.deleted', '=', 0)
                    ->whereIn('transparency.type', [1, 3])
                    ->get();
            }
            //! Ingles
            elseif ($req->type == 2) {
                $transparency = Transparency::select(
                    'transparency.id',
                    'transparency.is_active',
                    'transparency.description_eng',
                    'transparency.filename',
                    'transparency.administration',
                    'transparency.publish',
                    'transparency.created_at',
                    'transparency.created_by',

                )
                    ->where('transparency.publish', '=', 1)
                    ->where('transparency.deleted', '=', 0)
                    ->whereIn('transparency.type', [2, 3])
                    ->get();
            }
        } elseif ($req->publish == 0) {

            //! Español
            if ($req->type == 1) {
                $transparency = Transparency::select(
                    'transparency.id',
                    'transparency.fraction_id',
                    'transparency.department',
                    'transparency.filename',
                    'transparency.administration',
                    'transparency.publish',
                    'transparency.type',
                    'transparency.created_at',
                    'transparency.created_by',
                )
                    ->where('transparency.publish', '=', 0)
                    ->where('transparency.deleted', '=', 0)
                    ->whereIn('transparency.type', [1, 3])
                    ->get();
            }
            //! Ingles
            elseif ($req->type == 2) {
                $transparency = Transparency::select(
                    'transparency.id',
                    'transparency.is_active',
                    'transparency.description_eng',
                    'transparency.filename',
                    'transparency.administration',
                    'transparency.publish',
                    'transparency.type',
                    'transparency.created_at',
                    'transparency.created_by',

                )
                    ->where('transparency.publish', '=', 0)
                    ->where('transparency.deleted', '=', 0)
                    ->whereIn('transparency.type', [2, 3])
                    ->get();
            }
        } else {
            $response = ['success' => false, 'message' => 'No se encontraron transparencias activas de este lenguaje'];
        }

        if (count($transparency) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontraron transparencias activos';
        }
        $response['transparency'] = $transparency;
        $response['count'] = count($transparency);

        return response()->json($response);
    }
}
