<?php

namespace App\Http\Controllers;

use App\Models\Period;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PeriodController extends Controller
{

    /* todos los periodos */
    public function getAllPeriod()
    {
        $response = ['success' => false, 'message' => 'No se encontraron periodos activos'];

        $periods = Period::select(
            'period.id',
            'period.name',
        )
            ->get();

        if (count($periods) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontraron todos los periodos activos';
        }
        $response['Period'] = $periods;
        $response['count'] = count($periods);

        return response()->json($response);
    }
}
