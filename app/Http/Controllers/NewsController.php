<?php

namespace App\Http\Controllers;

use App\Models\News;
use Carbon\Carbon;
use Illuminate\Http\Request;

class NewsController extends Controller
{

    /* apis por post */

    /* crear noticia */ /* listo */
    public function createNew(Request $req)
    {

        try {
            News::create([
                'title' => $req->title,
                'description' => $req->description,
                'cover_page' => $req->cover_page,
                'multimedia' => $req->multimedia,
                'video' => $req->video,
                'link' => $req->link,
                'link_youtube' => $req->link_youtube,
                'department_id' => $req->departments,
                'type' => $req->type,
                'publish' => $req->publish,
                'date_validity' =>   Carbon::parse($req->date_validity),
                'created_at' => Carbon::now(),
                'created_by' =>   $req->created_by,
            ]);
            $response = ['success' => true, 'message' => 'Se creo la noticia'];
        } catch (\Throwable $e) {
            $response = ['success' => false, 'message' => $e];
            $response['message'] = "No se creo la noticia";
        }
        return $response;
    }

    /* modificar noticia */ /* listo */
    public function updateNew(Request $req)
    {
        //! Cuando todos vienen vacíos
        if ($req->cover_page != '' && $req->multimedia != '' && $req->video != '') {
            try {
                News::where('id', $req->id)->update([
                    'title' => $req->title,
                    'description' => $req->description,
                    'cover_page' => $req->cover_page,
                    'multimedia' => $req->multimedia,
                    'video' => $req->video,
                    'link' => $req->link,
                    'link_youtube' => $req->link_youtube,
                    'department_id' => $req->departments,
                    'type' => $req->type,
                    'is_active' => $req->is_active,
                    'publish' => $req->publish,
                    'date_validity' =>  $req->date_validity,
                    /* 'update_at' => Carbon::now(),
                    'update_by' =>   $req->update_by, */

                ]);
                $response = ['success' => true, 'message' => 'Se acualizo la noticia con video, imagen de portada y multimedia', 'lenguaje' => $req->type];
            } catch (\Throwable $e) {
                $response['success'] = false;
                $response['message'] = "No se actualizo la noticia correctamente";
                $response['error'] = $e->getMessage();
            }
            return $response;
        }
        //! Cuando el video viene vacío
        else if ($req->cover_page != '' &&  $req->multimedia != '') {
            try {
                News::where('id', $req->id)->update([
                    'title' => $req->title,
                    'description' => $req->description,
                    'cover_page' => $req->cover_page,
                    'multimedia' => $req->multimedia,
                    'link' => $req->link,
                    'link_youtube' => $req->link_youtube,
                    'department_id' => $req->departments,
                    'type' => $req->type,
                    'is_active' => $req->is_active,
                    'publish' => $req->publish,
                    'date_validity' =>  $req->date_validity,
                    /* 'update_at' => Carbon::now(),
                    'update_by' =>   $req->update_by, */
                ]);
                $response = ['success' => true, 'message' => 'Se acualizo la noticia con imagen de portada nueva y multimedia', 'lenguaje' => $req->type];
            } catch (\Throwable $e) {
                $response['success'] = false;
                $response['message'] = "No se actualizo la noticia correctamente";
                $response['error'] = $e->getMessage();
            }
        }
        //! Cuando la multimedia viene vacía
        else if ($req->cover_page != '' &&  $req->video != '') {
            try {
                News::where('id', $req->id)->update([
                    'title' => $req->title,
                    'description' => $req->description,
                    'cover_page' => $req->cover_page,
                    'video' => $req->video,
                    'link' => $req->link,
                    'link_youtube' => $req->link_youtube,
                    'department_id' => $req->departments,
                    'type' => $req->type,
                    'is_active' => $req->is_active,
                    'publish' => $req->publish,
                    'date_validity' =>  $req->date_validity,
                    /* 'update_at' => Carbon::now(),
                    'update_by' =>   $req->update_by, */
                ]);
                $response = ['success' => true, 'message' => 'Se acualizo la noticia con imagen de portada nueva y video', 'lenguaje' => $req->type];
            } catch (\Throwable $e) {
                $response['success'] = false;
                $response['message'] = "No se actualizo la noticia correctamente";
                $response['error'] = $e->getMessage();
            }
        }
        //! Cuando la portada viene vacía
        else if ($req->multimedia != '' &&  $req->video != '') {
            try {
                News::where('id', $req->id)->update([
                    'title' => $req->title,
                    'description' => $req->description,
                    'multimedia' => $req->multimedia,
                    'video' => $req->video,
                    'link' => $req->link,
                    'link_youtube' => $req->link_youtube,
                    'department_id' => $req->departments,
                    'type' => $req->type,
                    'is_active' => $req->is_active,
                    'publish' => $req->publish,
                    'date_validity' =>  $req->date_validity,
                    /* 'update_at' => Carbon::now(),
                    'update_by' =>   $req->update_by, */
                ]);
                $response = ['success' => true, 'message' => 'Se acualizo la noticia con imagen de portada nueva y video', 'lenguaje' => $req->type];
            } catch (\Throwable $e) {
                $response['success'] = false;
                $response['message'] = "No se actualizo la noticia correctamente";
                $response['error'] = $e->getMessage();
            }
        }
        //! Cuando video y multimedia vienen vacios
        else if ($req->cover_page != '') {
            try {
                News::where('id', $req->id)->update([
                    'title' => $req->title,
                    'description' => $req->description,
                    'cover_page' => $req->cover_page,
                    'link' => $req->link,
                    'link_youtube' => $req->link_youtube,
                    'department_id' => $req->departments,
                    'type' => $req->type,
                    'is_active' => $req->is_active,
                    'publish' => $req->publish,
                    'date_validity' =>  $req->date_validity,
                    /* 'update_at' => Carbon::now(),
                    'update_by' =>   $req->update_by, */
                ]);
                $response = ['success' => true, 'message' => 'Se acualizo la noticia con imagen de portada nueva', 'lenguaje' => $req->type];
                $response = ['type' => $req->type];
            } catch (\Throwable $e) {
                $response['success'] = false;
                $response['message'] = "No se actualizo la noticia correctamente";
                $response['error'] = $e->getMessage();
            }
        }
        //! Cuando video y portada vienen vacios
        else if ($req->multimedia != '') {
            try {
                News::where('id', $req->id)->update([
                    'title' => $req->title,
                    'description' => $req->description,
                    'multimedia' => $req->multimedia,
                    'link' => $req->link,
                    'link_youtube' => $req->link_youtube,
                    'department_id' => $req->departments,
                    'type' => $req->type,
                    'is_active' => $req->is_active,
                    'publish' => $req->publish,
                    'date_validity' =>  $req->date_validity,
                    /* 'update_at' => Carbon::now(),
                    'update_by' =>   $req->update_by, */
                ]);
                $response = ['success' => true, 'message' => 'Se acualizo la noticia con multimedia nueva', 'lenguaje' => $req->type];
            } catch (\Throwable $e) {
                $response['success'] = false;
                $response['message'] = "No se actualizo la noticia correctamente";
                $response['error'] = $e->getMessage();
            }
        }
        //! Cuando multimedia y portada vienen vacios
        else if ($req->video != '') {
            try {
                News::where('id', $req->id)->update([
                    'title' => $req->title,
                    'description' => $req->description,
                    'video' => $req->video,
                    'link' => $req->link,
                    'link_youtube' => $req->link_youtube,
                    'department_id' => $req->departments,
                    'type' => $req->type,
                    'is_active' => $req->is_active,
                    'publish' => $req->publish,
                    'date_validity' =>  $req->date_validity,
                    /* 'update_at' => Carbon::now(),
                    'update_by' =>   $req->update_by, */
                ]);
                $response = ['success' => true, 'message' => 'Se acualizo la noticia con video nuevo', 'lenguaje' => $req->type];
            } catch (\Throwable $e) {
                $response['success'] = false;
                $response['message'] = "No se actualizo la noticia correctamente";
                $response['error'] = $e->getMessage();
            }
        }
        //! Todos vacios
        else {
            try {
                News::where('id', $req->id)->update([
                    'title' => $req->title,
                    'description' => $req->description,
                    'link' => $req->link,
                    'link_youtube' => $req->link_youtube,
                    'department_id' => $req->departments,
                    'type' => $req->type,
                    'is_active' => $req->is_active,
                    'publish' => $req->publish,
                    'date_validity' =>  $req->date_validity,
                    /* 'update_at' => Carbon::now(),
                    'update_by' =>   $req->update_by, */
                ]);
                $response = ['success' => true, 'message' => 'Se acualizo el comunicado sin video, imagen de portada y multimedia nueva'];
            } catch (\Throwable $e) {
                $response['success'] = false;
                $response['message'] = "No se actualizo la noticia correctamente";
                $response['error'] = $e->getMessage();
            }
            return $response;
        }
    }

    /* borrar noticia */ /* listo */
    public function deleteNew(Request $req)
    {

        try {
            News::where('id', $req->id)->update([
                'deleted' => 1,
            ]);
            $response = ['success' => true, 'message' => 'Se elimino correctamente'];
        } catch (\Throwable $e) {
            $response['success'] = false;
            $response['message'] = "No se elimino correctamente";
        }
        return $response;
    }

    /* publicar noticia */ /* listo */
    public function publishNew(Request $req)
    {
        $response = ['success' => true, 'message' => 'Se publico correctamente'];

        try {
            News::where('id', $req->id)->update([
                'publish' => 1,
            ]);
        } catch (\Throwable $e) {
            $response['success'] = false;
            $response['message'] = "No se publico correctamente";
        }
        return $response;
    }

    /* despublicar noticia */ /* listo */
    public function unpublishNew(Request $req)
    {
        $response = ['success' => true, 'message' => 'Se despublico correctamente'];

        try {
            News::where('id', $req->id)->update([
                'publish' => 0,
            ]);
        } catch (\Throwable $e) {
            $response['success'] = false;
            $response['message'] = "No se despublico correctamente";
        }
        return $response;
    }

    /* apis por get */

    /* noticias filtradas por lenguaje */
    public function getBytype(Request $req)
    {
        $response = ['success' => false, 'message' => 'No se encontraron noticias activos'];

        //! Español
        if ($req->type == 1) {
            $news = News::select(
                'news.id',
                'news.title',
                'news.description',
                'news.cover_page',
                'news.multimedia',
                'news.publish',
                'news.created_at',
                'news.created_by',
            )
                ->where('news.deleted', '=', 0)
                ->where('news.type', '=', 1)
                ->get();
        }
        //! Ingles
        elseif ($req->type == 2) {
            $news = News::select(
                'news.id',
                'news.is_active',
                'news.description',
                'news.cover_page',
                'news.multimedia',
                'news.publish',
                'news.created_at',
                'news.created_by',

            )
                ->where('news.deleted', '=', 0)
                ->where('news.type', '=', 2)
                ->get();
        }
        //! Ambas
        else {
            $news = News::select(
                'news.id',
                'news.title',
                'news.description',
                'news.is_active',
                'news.cover_page',
                'news.multimedia',
                'news.publish',
                'news.type',
                'news.created_at',
                'news.created_by',
            )
                ->where('news.deleted', '=', 0)
                ->where('news.type', '=', 3)
                ->get();
        }

        if (count($news) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontraron noticias activos';
        }
        $response['news'] = $news;
        $response['count'] = count($news);

        return response()->json($response);
    }

    /* noticias publicadas por lenguaje */
    public function getBytypePublic(Request $req)
    {
        if ($req->publish == 1) {

            //! Español
            if ($req->type == 1) {
                $news = News::select(
                    'news.id',
                    'news.title',
                    'news.description',
                    'news.cover_page',
                    'news.multimedia',
                    'news.publish',
                    'news.created_at',
                    'news.created_by',
                )
                    ->where('news.publish', '=', 1)
                    ->where('news.deleted', '=', 0)
                    ->whereIn('news.type', [1, 3])
                    ->get();
            }
            //! Ingles
            elseif ($req->type == 2) {
                $news = News::select(
                    'news.id',
                    'news.is_active',
                    'news.description_eng',
                    'news.cover_page',
                    'news.multimedia',
                    'news.publish',
                    'news.created_at',
                    'news.created_by',

                )
                    ->where('news.publish', '=', 1)
                    ->where('news.deleted', '=', 0)
                    ->whereIn('news.type', [2, 3])
                    ->get();
            }
        } elseif ($req->publish == 0) {

            //! Español
            if ($req->type == 1) {
                $news = News::select(
                    'news.id',
                    'news.title',
                    'news.description',
                    'news.cover_page',
                    'news.multimedia',
                    'news.publish',
                    'news.type',
                    'news.created_at',
                    'news.created_by',
                )
                    ->where('news.publish', '=', 0)
                    ->where('news.deleted', '=', 0)
                    ->whereIn('news.type', [1, 3])
                    ->get();
            }
            //! Ingles
            elseif ($req->type == 2) {
                $news = News::select(
                    'news.id',
                    'news.is_active',
                    'news.description_eng',
                    'news.cover_page',
                    'news.multimedia',
                    'news.publish',
                    'news.type',
                    'news.created_at',
                    'news.created_by',

                )
                    ->where('news.publish', '=', 0)
                    ->where('news.deleted', '=', 0)
                    ->whereIn('news.type', [2, 3])
                    ->get();
            }
        } else {
            $response = ['success' => false, 'message' => 'No se encontraron noticias activas de este lenguaje'];
        }

        if (count($news) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontraron noticias activos';
        }
        $response['news'] = $news;
        $response['count'] = count($news);

        return response()->json($response);
    }

    /* todas las noticias */
    public function getAllNews()
    {
        $response = ['success' => false, 'message' => 'No se encontraron noticias activos'];

        $news = News::select(
            'news.id',
            'news.title',
            'news.description',
            'news.cover_page',
            'news.multimedia',
            'news.video',
            'news.link',
            'news.link_youtube',
            'news.department_id',
            'news.type',
            'news.is_active',
            'news.publish',
            'news.created_at',
            'news.created_by',
            'news.date_validity',
            'c.name as departments',
        )
            ->leftjoin('departments as c', 'news.department_id', '=', 'c.id')
            ->where('news.deleted', '=', 0)
            /* ->orderBy('created_at', 'DESC') */
            ->get();

        if (count($news) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontraron noticias activos';
        }
        $response['news'] = $news;
        $response['count'] = count($news);

        return response()->json($response);
    }

    /* noticias publicas */
    public function getNewsPublic()
    {
        $response = ['success' => false, 'message' => 'No se encontraron noticias activos'];

        $news = News::select(
            'news.id',
            'news.title',
            'news.description',
            'news.cover_page',
            'news.multimedia',
            'news.video',
            'news.link',
            'news.link_youtube',
            'news.department_id',
            'news.type',
            'news.is_active',
            'news.publish',
            'news.created_at',
            'news.created_by',
            'news.date_validity',
            'c.name as departments',
            'u.name as name_user',
        )
            ->leftjoin('departments as c', 'news.department_id', '=', 'c.id')
            ->leftjoin('users as u', 'news.created_by', '=', 'u.id')
            ->where('news.publish', '=', 1)
            ->where('news.deleted', '=', 0)
            /* ->orderBy('created_at', 'DESC') */
            ->get();

        if (count($news) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontraron noticias activos';
        }
        $response['news'] = $news;
        $response['count'] = count($news);

        return response()->json($response);
    }

    /* 4 noticia */
    public function getAllNewsLimit()
    {
        $response = ['success' => false, 'message' => 'No se encontraron noticias activos'];

        $news = News::select(
            'news.id',
            'news.title',
            'news.description',
            'news.cover_page',
            'news.multimedia',
            'news.video',
            'news.link',
            'news.link_youtube',
            'news.department_id',
            'news.type',
            'news.is_active',
            'news.publish',
            'news.created_at',
            'news.created_by',
            'news.date_validity',
            'c.name as departments',
        )
            ->leftjoin('departments as c', 'news.department_id', '=', 'c.id')
            ->where('news.type', '=', 1)
            ->where('news.publish', '=', 1)
            ->where('news.deleted', '=', 0)
            ->orderBy('id', 'DESC')
            ->limit(4)
            ->get();

        if (count($news) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontraron noticias activos';
        }
        $response['news'] = $news;
        $response['count'] = count($news);

        return response()->json($response);
    }

    /* noticas por id */
    public function getNewsById(Request $req)
    {
        $response = ['success' => false, 'message' => 'No se encontro la noticia '];

        $news = News::select(
            'news.id',
            'news.title',
            'news.description',
            'news.cover_page',
            'news.multimedia',
            'news.video',
            'news.link',
            'news.link_youtube',
            'news.department_id',
            'news.type',
            'news.is_active',
            'news.publish',
            'news.deleted',
            'news.created_at',
            'news.created_by',
            'news.date_validity',
            'c.name as departments',
            'u.name as name_user',
        )
            ->leftjoin('departments as c', 'news.department_id', '=', 'c.id')
            ->leftjoin('users as u', 'news.created_by', '=', 'u.id')
            ->where('news.id', $req->id)
            ->where('news.deleted', '=', 0)
            ->get();



        if (count($news) > 0) {
            $response['success'] =  true;
            $response['message'] = 'Se encontro la noticia';
        }
        $response['news'] = $news;
        $response['count'] = count($news);

        return response()->json($response);
    }
}
